package com.example.android_test_app.text;

import java.util.ArrayList;
import java.util.List;

public class TextParser {

    /** Массив блоков текста */
    private List<String> blocks = new ArrayList<>();

    /** Оригинальный текст */
    private StringBuilder original_text;

    /** Признак разделения блоков
     * todo спросить у переводчика нормальное имя переменной */
    private CharSequence del = "\n";

    /** Для создания парсера по разделителю */
    public TextParser(CharSequence original_text, CharSequence del) {
        this.original_text = new StringBuilder(original_text);
        this.del = del;
    }

    /** Для создания парсера с дефолтным разделителем */
    public TextParser(CharSequence original_text){
        this.original_text = new StringBuilder(original_text);
    }
}
